// Datos de creación
var data = {
    name: 'Arbol',
    children: [
      { name: 'primer elemento' },
      { name: 'segundo elemento' },
      {
        name: 'folder 1',
        children: [
          {
            name: 'folder 2',
            children: [
              { name: 'tercer elemento' },
              { name: 'cuarto elemento' }
            ]
          },
          { name: 'quinto elemento' },
          { name: 'sexto elemento' },
          {
            name: 'folder 3',
            children: [
              { name: 'septimo elemento' },
              { name: 'octavo elemento' }
            ]
          }
        ]
      }
    ]
  }

  Vue.component('item', {
    template: ` <div>
    <li>
    <div
        :class="{bold: isFolder}"
        @click="toggle"
        @dblclick="changeType">
        {{model.name}}
        <span v-if="isFolder">[{{open ? '-' : '+'}}]</span>
      </div>
      <ul v-show="open" v-if="isFolder">
        <item
          class="item"
          v-for="model in model.children"
          :model="model">
      </item>
        <li class="add" @click="addChild">+</li>
      </ul>
    </li>
    
    </div>`
    
    
    ,
    props: {
      model: Object
    },
    data: function () {
      return {
        open: false
      }
    },
    computed: {
      isFolder: function () {
        return this.model.children &&
          this.model.children.length
      }
    },
    methods: {
      toggle: function () {
        if (this.isFolder) {
          this.open = !this.open
        }
      },
      changeType: function () {
        if (!this.isFolder) {
          Vue.set(this.model, 'children', [])
          this.addChild()
          this.open = true
        }
      },
      addChild: function () {
        this.model.children.push({
          name: 'new stuff'
        })
      }
    }
  })
  
  // boot up the demo
  var demo = new Vue({
    el: '#demo',
    data: {
      treeData: data
    }
  })


